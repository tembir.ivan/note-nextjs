type User = {
  name: string;
  email: string;
  password: string;
};

type Notion = {
  title: string;
  text: string;
  date: string;
  assign: string;
};

export type { User, Notion };
