import Link from 'next/link'

export default function NotFound() {
    return (
        <div className='w-full h-full flex flex-col items-center justify-center'>
            <h2>Not Found</h2>
            <p>Could not find requested resource</p>
            <Link className='text-[lightblue]' href="/">Return Home</Link>
        </div>
    )
}