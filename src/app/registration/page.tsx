import LoginLogo from "@/shared/ui/components/atoms/Login-logo"
import RegisterForm from "@/shared/ui/components/molecules/Register-form"
import { Metadata } from "next";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";


export const metadata: Metadata = {
    title: "Registration",
};


const Registration = async () => {
    const session = await getServerSession();
    session !== null && redirect("/notion")

    return (
        <section className="flex w-full min-h-screen justify-center items-center">
            <div className="py-3 flex flex-col justify-center  max-w-[440px] max-md:p-3 max-md:max-w-full max-md:text-center">
                <LoginLogo />
                <span className="mt-[30px] text-[32px] font-semibold">Create a new account</span>
                <p className="mt-2.5 text-greylight">
                    {"It's"} not difficult, you just need to enter some data and {"it's"} ready to go!
                </p>
                <RegisterForm />
                <p className="text-[#4B5563] font-semibold mt-5">By registering you agree to the Terms of Service and Privacy Policy of Namanyajugabelajar.io</p>
            </div>
        </section >
    )
}

export default Registration