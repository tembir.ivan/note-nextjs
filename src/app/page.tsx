import { getServerSession } from "next-auth";
import Link from "next/link";

export default async function Page() {
  const session = await getServerSession();

  return <><p className="text-center">Signed in as {session!.user!.name}</p>
    <Link href={"/notion"} className="block w-full text-[blue] text-center">Go to the blogs page</Link></>;
}
