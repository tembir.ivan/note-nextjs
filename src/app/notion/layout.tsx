import SignOut from "@/shared/ui/components/atoms/SignOut";
import MobileMenu from "@/shared/ui/components/molecules/MobileMenu";
import { Metadata } from "next";

export const metadata: Metadata = {
    title: "Notion",
};

export default function NotionsLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <section className="h-screen overflow-x-hidden overflow-y-scroll">
            <div className="flex flex-col p-10 relative h-full">
                <SignOut status="desktop" />
                <MobileMenu />
                {children}
            </div>
        </section>
    );
}