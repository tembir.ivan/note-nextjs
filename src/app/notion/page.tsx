import CompleteIcon from "@/shared/ui/components/atoms/Icons/CompleteIcon";
import ProgressIcon from "@/shared/ui/components/atoms/Icons/ProgressIcon";
import TodoIcon from "@/shared/ui/components/atoms/Icons/TodoIcon";
import NotionCardSkeleton from "@/shared/ui/components/atoms/NotionCardSkeleton";
import BoardStatus from "@/shared/ui/components/molecules/BoardStatus";
import { getServerSession } from "next-auth";
import { Suspense } from "react";

const data = [
    { status: "To-do", icon: <TodoIcon width={16} height={16} className="mr-[7px]" /> },
    { status: "In Progress", icon: <ProgressIcon width={16} height={16} className="mr-[7px]" /> },
    { status: "Complete", icon: <CompleteIcon width={16} height={16} className="mr-[7px]" /> }
]

const Page = async ({ searchParams }: { searchParams: { status: string } }) => {
    const user = await getServerSession()

    return (
        <>
            <span className="text-midnight font-bold text-[32px]">Notion Board</span>
            <p className="mt-5 text-greylight text-sm tracking-[0.1px]">A board to keep track of personal tasks.</p>
            <div className="mt-[34px] flex justify-between max-md:hidden">
                {data.map((elem: any, index: any) => <Suspense key={index} fallback={<NotionCardSkeleton />}><BoardStatus email={user!.user?.email!} {...elem} /></Suspense>)
                }
            </div>
            <div className="md:hidden mt-[34px]">{searchParams.status !== undefined && <Suspense fallback={<NotionCardSkeleton />}><BoardStatus email={user!.user?.email!} {...data.find((elem: any) => elem.status === searchParams.status)!} /></Suspense>}</div>
        </>
    )
}

export default Page