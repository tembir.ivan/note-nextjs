"use client"
import AssignIcon from "@/shared/ui/components/atoms/Icons/AssignIcon"
import ClockIcon from "@/shared/ui/components/atoms/Icons/ClockIcon"
import CreatorIcon from "@/shared/ui/components/atoms/Icons/CreatorIcon"
import StatusIcon from "@/shared/ui/components/atoms/Icons/StatusIcon"
import { getSession } from "next-auth/react"
import Link from "next/link"
import { useRouter, useSearchParams } from "next/navigation"
import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"

type Inputs = {
    textarea: string,
    status: "To-do" | "In Progress" | "Complete",
    assign: string,
}

const Page = () => {
    const router = useRouter();
    const {
        register,
        watch,
        formState: { errors },
    } = useForm<Inputs>()
    const searchParams = useSearchParams();
    const [session, setSession] = useState<string>()
    const [users, setUsers] = useState<string[]>()
    const date = new Date().toLocaleDateString("en-US", { "month": "long", "day": "numeric", "year": "numeric", "hour": 'numeric', "minute": "numeric" });

    useEffect(() => {
        fetch("./api/users").then(res => res.json()).then((value: string[]) =>
            setUsers(value)
        )

        getSession().then(res => setSession(res?.user?.name!));

        const handleReload = async () => {
            const data = { date, ...watch() }
            await fetch("./api/notion", {
                'method': "POST", "body": JSON.stringify(data), headers: {
                    'Content-Type': 'application/json',
                },
            })
        }

        window.addEventListener('beforeunload', handleReload)

        return () => window.removeEventListener('beforeunload', handleReload)
    }, [])

    const handleLink = async () => {
        if (watch().textarea !== "") {
            const data = { date, ...watch() }
            await fetch("./api/notion", {
                'method': "POST", "body": JSON.stringify(data), headers: {
                    'Content-Type': 'application/json',
                },
            })
            router.refresh()
        }
    }

    return (
        <>
            <Link href={"/notion"} onClick={handleLink} className="leading-[38.40px] text-[#37352F] text-[31px] font-bold">Notions Page</Link>
            <form className="mt-[25px]" method="POST">
                <div className="grid grid-cols-2 gap-y-3.5 text-[#37352F] text-[13px] max-[425px]:flex max-[425px]:flex-col gap-0">
                    <span className="flex items-center opacity-60">
                        <AssignIcon width={16} height={10} className="mr-1.5" />Assign
                    </span>
                    <span>
                        <select className="opacity-60" defaultValue={"Empty"} {...register("assign")}>
                            <option value="Empty">Empty</option>
                            {users?.map((elem: string, index: number) => <option value={elem} key={index}>{elem}</option>)}
                        </select>
                    </span>
                    <span className="flex items-center opacity-60">
                        <StatusIcon width={16} height={16} className="mr-1.5" />Status
                    </span>
                    <span>
                        <select className="opacity-60" defaultValue={searchParams.get("status")!} {...register("status")}>
                            <option value="To-do">To-do</option>
                            <option value="In Progress">In Progress</option>
                            <option value="Complete">Complete</option>
                        </select>
                    </span>
                    <span className="flex items-center opacity-60">
                        <CreatorIcon width={16} height={16} className="mr-1.5" /> Created for
                    </span>
                    <span>
                        {session}
                    </span>
                    <span className="flex items-center opacity-60">
                        <ClockIcon width={16} height={16} className="mr-1.5" /> Created at
                    </span>
                    <span>
                        {date}
                    </span>
                </div>
                <span className="mt-[17px] w-full h-px bg-[#37352F] bg-opacity-10 block"></span>
                <textarea {...register("textarea")} className="mt-10 w-full resize-none text-[black] outline-none border-none h-full p-5 bg-[#F8F8F8] first-line:font-bold first-line:text-[40px] first-line:leading-[44px] font-light text-sm"></textarea>
            </form>
        </>
    )
}

export default Page;