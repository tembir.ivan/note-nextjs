import Page from "./page";

export async function generateMetadata() {
    return { title: "Create | Notes" }
}

export default Page
