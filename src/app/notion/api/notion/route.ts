import { collection, deleteDoc, doc, setDoc } from "firebase/firestore";
import { db } from "../../../../../firebase.config";
import { getServerSession } from "next-auth";
import { revalidatePath } from "next/cache";

export async function POST(req: Request) {
  const body = await req.json();
  console.log(body);
  const textarea: string = body.textarea;
  const title: string = textarea.split("\n")[0];
  const text: string = textarea.includes("\n")
    ? textarea.slice(textarea.indexOf("\n") + 1)
    : "";
  const session = await getServerSession();
  const email: string = session?.user?.email!;
  const ref = collection(db, `users`, email, body.status);
  if (body.pastId || body.pastStatus === body.status) {
    const ref = collection(db, `users`, session?.user?.email!, body.pastStatus);
    await deleteDoc(doc(ref, body.pastId));
  }
  await setDoc(doc(ref, title), {
    title: title,
    text: text,
    date: body.date,
    assign: body.assign,
  });
  revalidatePath("/notion");
  return Response.json(body);
}

export async function DELETE(req: Request) {
  const body = await req.json();
  const session = await getServerSession();
  const ref = collection(db, `users`, session?.user?.email!, body.status);
  await deleteDoc(doc(ref, body.title));
  return Response.json(body);
}
