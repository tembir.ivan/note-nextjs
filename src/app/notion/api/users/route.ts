import { collection, getDocs } from "firebase/firestore";
import { db } from "../../../../../firebase.config";
import { getServerSession } from "next-auth";

export async function GET() {
  const session = await getServerSession();
  const docRef = collection(db, "users");
  const docSnap = await getDocs(docRef);
  const usersName: string[] = [];
  docSnap.forEach((doc) => {
    usersName.push(doc.data().name);
  });
  usersName.splice(usersName.indexOf(session?.user?.name!), 1);
  return Response.json(usersName);
}
