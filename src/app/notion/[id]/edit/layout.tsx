import Page from "./page"

export async function generateMetadata({ params: { id } }: { params: { id: string } }) {
    return {
        title: `${id} | Edit`,
    }
}

export default Page