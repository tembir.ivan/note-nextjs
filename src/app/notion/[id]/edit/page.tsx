"use client"
import AssignIcon from "@/shared/ui/components/atoms/Icons/AssignIcon"
import ClockIcon from "@/shared/ui/components/atoms/Icons/ClockIcon"
import CreatorIcon from "@/shared/ui/components/atoms/Icons/CreatorIcon"
import StatusIcon from "@/shared/ui/components/atoms/Icons/StatusIcon"
import { collection, doc, getDoc } from "firebase/firestore"
import { getSession } from "next-auth/react"
import Link from "next/link"
import { useRouter, useSearchParams } from "next/navigation"
import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { db } from "../../../../../firebase.config"
import { Notion } from "@/entities/types"
import { DocumentData } from "firebase-admin/firestore"

type Inputs = {
    textarea: string,
    status: "To-do" | "In Progress" | "Complete",
    assign: string,
}

const Page = ({ params }: { params: { id: string } }) => {
    const router = useRouter();
    const [loader, setLoader] = useState<boolean>(true)
    const [pastId, setPastId] = useState<string>()
    const [date, setDate] = useState<string>()
    const [session, setSession] = useState<string>()
    const [users, setUsers] = useState<string[]>()
    const searchParams = useSearchParams();
    const {
        register,
        watch,
        setValue,
        formState: { errors },
    } = useForm<Inputs>()
    const id = decodeURI(params.id)


    useEffect(() => {
        fetch("../api/users").then(res => res.json()).then((value: string[]) =>
            setUsers(value)
        ).then(() => getSession().then(res => {
            setSession(res?.user?.name!);

            getNotion(id, searchParams.get("status")!, res?.user?.email!).then((elem: Notion | DocumentData | undefined) => {
                if (elem) {
                    setValue('textarea', `${elem.title}\n` + elem.text)
                    setValue('status', searchParams.get('status')! as "To-do" | "In Progress" | "Complete");
                    setValue('assign', elem.assign)
                    setDate(elem.date)
                    setPastId(elem.title)
                    setLoader(false)
                }
            })
        }))
    }, [])

    useEffect(() => {
        const handleReload = async () => {
            const data = { date, pastId, pastStatus: searchParams.get('status'), ...watch() }
            await fetch("../api/notion", {
                'method': "POST", "body": JSON.stringify(data), headers: {
                    'Content-Type': 'application/json',
                },
            })
        }

        window.addEventListener('beforeunload', handleReload)

        return () => window.removeEventListener('beforeunload', handleReload)
    }, [loader])

    const handleLink = async () => {
        const data = { date, pastId, pastStatus: searchParams.get('status'), ...watch() }
        if (watch().textarea !== "") {
            await fetch("../api/notion", {
                'method': "POST", "body": JSON.stringify(data), headers: {
                    'Content-Type': 'application/json',
                },
            })
            router.refresh()
        }
    }
    return (<>
        <Link href={"/notion"} onClick={handleLink} className="leading-[38.40px] text-[#37352F] text-[31px] font-bold">Notions Page</Link>
        {!loader ? <form className="mt-[25px]" method="POST">
            <div className="grid grid-cols-2 gap-y-3.5 text-[#37352F] text-[13px] max-[425px]:flex max-[425px]:flex-col gap-0">
                <span className="flex items-center opacity-60">
                    <AssignIcon width={16} height={10} className="mr-1.5" />Assign
                </span>
                <span>
                    <select className="opacity-60" {...register("assign")}>
                        <option value="Empty">Empty</option>
                        {users?.map((elem: string, index: number) => <option value={elem} key={index}>{elem}</option>)}
                    </select>
                </span>
                <span className="flex items-center opacity-60">
                    <StatusIcon width={16} height={16} className="mr-1.5" />Status
                </span>
                <span>
                    <select className="opacity-60" {...register('status')}>
                        <option value="To-do">To-do</option>
                        <option value="In Progress">In Progress</option>
                        <option value="Complete">Complete</option>
                    </select>
                </span>
                <span className="flex items-center opacity-60">
                    <CreatorIcon width={16} height={16} className="mr-1.5" /> Created for
                </span>
                <span>
                    {session}
                </span>
                <span className="flex items-center opacity-60">
                    <ClockIcon width={16} height={16} className="mr-1.5" /> Created at
                </span>
                <span>
                    {date}
                </span>
            </div>
            <span className="mt-[17px] w-full h-px bg-[#37352F] bg-opacity-10 block"></span>
            <textarea {...register("textarea")} className="mt-10 w-full resize-none text-[black] outline-none border-none h-full p-5 bg-[#F8F8F8] first-line:font-bold first-line:text-[40px] first-line:leading-[44px] font-light text-sm"></textarea>
        </form> : <span>Loading...</span>} </>
    )
}

export default Page

const getNotion = async (id: string, status: string, email: string): Promise<Notion> => {
    const ref = collection(db, `users`, email, status);
    const docRef = doc(ref, id);
    const notion = await getDoc(docRef);
    return notion.data() as Notion;
};