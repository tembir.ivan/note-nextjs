import LoginLogo from "@/shared/ui/components/atoms/Login-logo";
import LoginForm from "@/shared/ui/components/molecules/Login-form";
import { Metadata } from "next";
import { getServerSession } from "next-auth";
import Link from "next/link";
import { redirect } from "next/navigation";
import React from "react";

export const metadata: Metadata = {
  title: "Login",
};

const Page = async () => {
  const session = await getServerSession();
  session !== null && redirect("/notion")

  return (
    <section className="flex w-full min-h-screen justify-center items-center">
      <div className="py-3 flex flex-col justify-center max-w-[440px] max-md:p-3 max-md:max-w-full max-md:text-center">
        <LoginLogo />
        <span className="mt-[30px] text-[32px] font-semibold">Log in to your account</span>
        <p className="mt-2.5 text-greylight">
          Learn for free at Namanyajugabelajar.io, and start the career
          {"you've"}
          been dreaming of since you were an embryo!
        </p>
        <LoginForm />
        <p className="text-[#4B5563] font-semibold mt-5">{"Don't"} have an account yet? <Link className="text-[#4F46E5]" href={"/registration"}>Register now, {"it's"} free!</Link></p>
      </div>
    </section>
  );
};

export default Page;
