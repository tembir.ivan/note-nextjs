"use client"

import { useEffect } from 'react'

export default function Error({
    error,
    reset,
}: {
    error: Error & { digest?: string }
    reset: () => void
}) {

    return (
        <div className='w-full h-full flex flex-col items-center justify-center'>
            <h2>Something went wrong!</h2>
            <p className='text-[red]'>Error: {error.message}</p>
            <button className="text-[lightblue]"
                onClick={
                    () => reset()
                }
            >
                Try again
            </button>
        </div>
    )
}

