export { default } from "next-auth/middleware";

export const config = {
  matcher: ["/notion/:path*", "/create", "/"],
};
