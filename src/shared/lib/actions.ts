"use server";
import { z } from "zod";
import { createUser } from "./firebaseFunc";
import { redirect } from "next/navigation";

export const registration = async (formData: FormData) => {
  const dataToAuth = Object.fromEntries(formData);

  const parsedCredentials = z
    .object({
      email: z.string().email(),
      name: z.string().min(3),
      password: z.string().min(6),
    })
    .safeParse(dataToAuth);

  if (parsedCredentials.success) {
    await createUser(parsedCredentials.data);
    redirect("/login");
  }
  return null;
};
