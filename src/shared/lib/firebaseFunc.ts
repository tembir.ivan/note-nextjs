import { collection, doc, getDoc, getDocs, setDoc } from "firebase/firestore";
import { db } from "../../../firebase.config";
import { Notion, User } from "@/entities/types";
import { revalidatePath } from "next/cache";
const bcrypt = require("bcrypt");

const getUser = async (email: string) => {
  const docRef = doc(db, "users", email);
  const docSnap = await getDoc(docRef);
  return docSnap.data();
};

const createUser = async (userData: User) => {
  const userDB = await getUser(userData.email);
  if (!userDB) {
    const passwordHash: string = bcrypt.hashSync(userData.password, 10);
    try {
      const usersRef = collection(db, "users");
      await setDoc(doc(usersRef, userData.email), {
        ...userData,
        id: Date.now(),
        password: passwordHash,
      });
    } catch (error) {
      throw error;
    }
  }
};

const getNotions = async (userEmail: string, status: string) => {
  const ref = collection(db, `users/${userEmail}`, status);
  const notions = await getDocs(ref);
  const result: Notion[] = [];
  notions.forEach((elem) => {
    result.push(elem.data() as Notion);
  });
  revalidatePath("/notion");
  return result;
};

export { getUser, createUser, getNotions };
