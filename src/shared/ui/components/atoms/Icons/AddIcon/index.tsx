import { ComponentProps } from "react"

type Props = ComponentProps<'svg'>

const AddIcon = ({ ...props }: Props) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13" fill="none" {...props}>
            <g clipPath="url(#clip0_1_1170)">
                <path d="M2.12534 6.5L11.1253 6.5" stroke="#A3A3A3" strokeWidth="2" strokeLinecap="round" />
                <line x1="7.12534" y1="2.5" x2="7.12534" y2="10.5" stroke="#A3A3A3" strokeWidth="2" strokeLinecap="round" />
            </g>
            <defs>
                <clipPath id="clip0_1_1170">
                    <rect width="12.032" height="12" fill="white" transform="translate(0.634674 0.5)" />
                </clipPath>
            </defs>
        </svg>
    )
}

export default AddIcon