import { ComponentProps } from "react"

type Props = ComponentProps<'svg'>

const BurgerIcon = ({ ...props }: Props) => {
    return (
        <svg width="37" height="33" viewBox="0 0 37 33" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
            <line x1="4.125" y1="29.5" x2="33.125" y2="29.5" stroke="#333333" strokeWidth="7" strokeLinecap="round" />
            <line x1="4.125" y1="16.5" x2="33.125" y2="18.5211" stroke="#333333" strokeWidth="7" strokeLinecap="round" />
            <line x1="4.125" y1="3.5" x2="33.125" y2="3.5" stroke="#333333" strokeWidth="7" strokeLinecap="round" />
        </svg>

    )
}

export default BurgerIcon