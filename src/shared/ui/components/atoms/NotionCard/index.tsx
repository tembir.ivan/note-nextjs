"use client"
import Link from "next/link"
import { useRouter } from "next/navigation"
import React from "react"

const NotionCard = ({ title, text, status }: { title: string, text: string, status: string }) => {
    const router = useRouter()

    const handleContextMenu = async (event: React.MouseEvent) => {
        event.preventDefault();
        const del: boolean = confirm(`Delete "${title}" notion`)
        if (del) {
            const response = await fetch("/notion/api/notion",
                { 'method': "DELETE", body: JSON.stringify({ title, status }) })
            if (response.ok) router.refresh()
        };
    }

    return (
        <Link onContextMenu={handleContextMenu} href={`/notion/${title}/edit?status=${status}`} className="p-4 mt-2 h-[113px] flex flex-col rounded-lg border border-solid border-[#ddd] shadow-midnight shadow-[0 2px 4px 0] first:mt-0 cursor-pointer"><span className="text-midnight text-[14px] font-bold tracking-[-0.2px]">{title}</span>
            <p className="mt-2  whitespace-break-spaces text-greylight text-[12px] tracking-[-0.2px]">{text.split(' ').length > 4 ? <>{text.split(' ').splice(0, 4).join(' ')}...</> : text.split('\n').length > 4 ? <> {text.split('\n').splice(0, 4).join(' ')}... </> : text}</p>
        </Link>
    )
}

export default NotionCard