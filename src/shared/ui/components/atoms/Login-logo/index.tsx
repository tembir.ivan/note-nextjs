const LoginLogo = () => {
  return <span className="flex justify-center items-center w-[50px] h-[50px] rounded-xl bg-[#EEF2FF] text-[#4F46E5] text-[28px] font-semibold max-md:self-center">G</span>;
};

export default LoginLogo;
