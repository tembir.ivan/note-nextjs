const NotionCardSkeleton = () => {
    return (
        <div contextMenu={"handleContextMenu"} className="w-full p-4 mt-2 h-[113px] flex flex-col rounded-lg border border-solid border-[#ddd] shadow-midnight shadow-[0 2px 4px 0] first:mt-0 cursor-pointer">
        </div>
    )
}

export default NotionCardSkeleton