"use client"
import clsx from "clsx"
import { signOut } from "next-auth/react"

type Props = { status: "mobile" | "desktop" };

const SignOut = ({ status }: Props) => {
    return (
        <button onClick={() => signOut()} className={clsx("w-40 h-10 text-ocean border border-solid rounded-md", { "self-center mt-5": status === "mobile", "absolute right-10 max-md:hidden": status === "desktop" })}>Sign Out</button>
    )
}

export default SignOut