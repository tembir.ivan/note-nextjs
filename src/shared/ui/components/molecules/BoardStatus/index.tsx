import Link from "next/link";
import AddIcon from "../../atoms/Icons/AddIcon";
import clsx from "clsx";
import { getNotions } from "@/shared/lib/firebaseFunc";
import { DocumentData } from "firebase/firestore";
import { Notion } from "@/entities/types";
import { Fragment } from "react";
import NotionCard from "../../atoms/NotionCard";

type Props = {
    status: string,
    icon: React.ReactElement,
    email: string
}

const BoardStatus = async ({ status, icon, email }: Props) => {
    const data: DocumentData | undefined = await getNotions(email, status);
    const notions: Notion[] = data !== undefined ? Object.values(data) : [];
    const amount: number = notions.length;

    return (
        <div className="flex flex-col ml-6 first:ml-0 min-w-[210px] w-full">
            <div className="flex justify-between items-center">
                <span className={clsx('flex items-center text-xs leading-[21px] font-medium', {
                    "text-[#787774]": status === "To-do",
                    'text-ocean': status === 'In Progress',
                    'text-swamp': status === 'Complete',
                })}>{icon} {status} {amount}</span>
                <Link href={`/notion/create?status=${status}`}><AddIcon width={20} height={20} /></Link>
            </div>
            <div className="p-3 flex flex-col">{notions.map((elem: Notion, index: number) => <Fragment key={index}><NotionCard status={status} {...elem} /></Fragment>)}</div>
        </div >
    )
}

export default BoardStatus