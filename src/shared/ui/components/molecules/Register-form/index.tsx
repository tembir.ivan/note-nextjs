import { registration } from "@/shared/lib/actions"

const RegisterForm = () => {
    return (
        <form action={registration} className="mt-[30px]">
            <label htmlFor="name" className="font-semibold flex flex-col items-start">Name
                <input id="name" type="text" required minLength={3} name="name" className="px-10 py-5 bg-[#F3F4F6] w-full h-[65px] mt-2.5 rounded-lg focus:border-ocean focus:border-[1px] focus:border-solid" />
            </label>
            <label htmlFor="email" className="font-semibold flex flex-col items-start mt-[15px]">Email
                <input id="email" type="email" required name="email" className="px-10 py-5 bg-[#F3F4F6] w-full h-[65px] mt-2.5 rounded-lg focus:border-ocean focus:border-[1px] focus:border-solid" />
            </label>
            <label htmlFor="password" className="mt-[15px] font-semibold flex flex-col items-start">Password
                <input id="password" type="password" minLength={6} required name="password" className="px-10 py-5 bg-[#F3F4F6] w-full h-[65px] mt-2.5 rounded-lg focus:border-ocean focus:border-[1px] focus:border-solid" />
            </label>
            <button className=" text-[white] w-full h-[50px] mt-[30px] rounded-md bg-[#4F46E5] shadow-[#4f46e526] shadow-md ">Sign Up</button>
        </form>
    )
}

export default RegisterForm