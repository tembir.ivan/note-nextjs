"use client"
import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState } from "react";

type Form = {
    email: string,
    password: string,
}

const LoginForm = () => {
    const navigation = useRouter()
    const [formValues, setFormValues] = useState<Form>({
        email: "",
        password: "",
    });
    const [error, setError] = useState<string>()

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setError(undefined)
        const { name, value } = event.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleAuth = async (event: React.FormEvent) => {
        event.preventDefault();
        const response = await signIn('credentials', { redirect: false, ...formValues })
        if (response!.ok) navigation.push('/notion')
        if (response?.error) setError(response.error)
    }

    return (
        <form className="mt-[30px]" onSubmit={handleAuth}>
            {error && <p className="text-center bg-[red] py-4 mb-6 rounded">{error}</p>}
            <label htmlFor="email" className="font-semibold flex flex-col items-start" >Email
                <input id="email" type="email" required name="email" className="px-10 py-5 bg-[#F3F4F6] w-full h-[65px] mt-2.5 rounded-lg focus:border-ocean focus:border-[1px] focus:border-solid" value={formValues.email}
                    onChange={handleChange} />
            </label>
            <label htmlFor="password" className="mt-[30px] font-semibold flex flex-col items-start" >Password
                <input id="password" type="password" minLength={6} required name="password" className="px-10 py-5 bg-[#F3F4F6] w-full h-[65px] mt-2.5 rounded-lg focus:border-ocean focus:border-[1px] focus:border-solid" value={formValues.password}
                    onChange={handleChange} />
            </label>
            <button className="text-[white] w-full h-[50px] mt-[30px] rounded-md bg-[#4F46E5] shadow-[#4f46e526] shadow-md">Log In</button>
        </form>
    )
}

export default LoginForm