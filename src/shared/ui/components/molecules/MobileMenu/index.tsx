"use client"
import React, { useEffect, useState } from 'react'
import BurgerIcon from '../../atoms/Icons/BurgerIcon'
import Link from 'next/link'
import SignOut from '../../atoms/SignOut'
import clsx from 'clsx'

const MobileMenu = () => {
    const [open, setOpen] = useState<boolean>(false)


    return (
        <>
            <BurgerIcon width={24} height={24} className='md:hidden absolute right-3 z-10 cursor-pointer' onClick={() => setOpen(e => !e)} />
            {<ul className={clsx('transition-all duration-200 md:hidden bg-[white] w-full h-screen absolute z-[2] flex flex-col items-center justify-center top-0 left-0', { "translate-x-0": open, "translate-x-[800px]": !open })}>
                <li><Link href={"/notion?status=To-do"} className='text-ocean focus:text-swamp'>To-do</Link></li>
                <li><Link href={"/notion?status=In Progress"} className='text-ocean focus:text-swamp'>In Progress</Link></li>
                <li><Link href={"/notion?status=Complete"} className='text-ocean focus:text-swamp'>Complete</Link></li>
                <li><SignOut status="mobile" /></li>
            </ul>}
        </>
    )
}

export default MobileMenu