import { getUser } from "@/shared/lib/firebaseFunc";
import NextAuth, { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
const bcrypt = require("bcrypt");

export const authConfig: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      name: "credentials",
      credentials: {
        email: { label: "email", type: "email", required: true },
        password: { label: "password", type: "password", required: true },
        rememberMe: { label: "rememberMe", type: "checkbox", required: false },
      },
      async authorize(credentials) {
        if (credentials?.email || credentials?.password) {
          const user = await getUser(credentials.email);
          if (!user) return null;
          const passwordMatch = await bcrypt.compare(
            credentials.password,
            user.password
          );
          if (passwordMatch)
            return {
              id: user.id,
              email: user.email,
              name: user.name,
              rememberMe: credentials.rememberMe,
            };
        }
        return null;
      },
    }),
  ],
  callbacks: {
    jwt: ({ token, user }) => {
      if (user) {
        const u = user as any;
        return {
          ...token,
          id: u.id,
          randomKey: u.randomKey,
        };
      }
      return token;
    },
    async session({ session, token, user }) {
      return session;
    },
  },
  session: {
    strategy: "jwt",
  },
  pages: { signIn: "/login" },
  secret: process.env.NEXTAUTH_SECRET,
};
