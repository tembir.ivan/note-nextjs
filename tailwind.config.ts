import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/shared/ui/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      midnight: "#212121",
      greylight: "#5A5A65",
      ocean: "#337EA9",
      swamp: "#448361",
    },
  },
  plugins: [],
};
export default config;
